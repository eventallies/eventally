package com.example.tamer.eventally;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Tamer on 4/28/2015.
 */
public class ListRow{
    String event_id;
    String creator_id;
    String title;
    String date;
    String time;
    String discripton;
    String category;
    public ListRow(String title,String date,String discripton,String creator_id,String category,String time,String event_id){
        this.title=title;
        this.date=date;
        this.discripton=discripton;
        this.creator_id=creator_id;
        this.category=category;
        this.time=time;
        this.event_id=event_id;
    }
}
