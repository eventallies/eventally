package com.example.tamer.eventally;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.Profile;
import com.melnykov.fab.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import Adapters.ViewPagerAdapter;
import Tabs.SlidingTabLayout;


public class home_screen extends ActionBarActivity{
    private ListView lv_sheep;
    ArrayList<ListRow> values;
    String TITLES[] = {"All Events","My Events","Sports","Lan Party","Shopping","Study Group","Settings","Log out"};
    int ICONS[] = {R.mipmap.ic_cal,R.mipmap.ic_star,R.mipmap.drawer_sports,R.mipmap.drawer_game,R.mipmap.drawer_shopping,R.mipmap.drawer_study,R.mipmap.drawer_settings,R.mipmap.ic_logout};
    String NAME = "Guest";
    String PROFILE;
    Profile profile;
    static Boolean guestMode;

    //tabs stuff
    ViewPager pager;
    ViewPagerAdapter pager_adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"All Events","My Events","Invitations"};
    int Numboftabs =3;
    // end of tabs stuff


    android.support.v7.widget.Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    ActionBarDrawerToggle mDrawerToggle;
    MyAdapter1 adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        // tabs stuff
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        pager_adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pager_adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
        //tabs stuff

        /*Bundle extras;
        if(savedInstanceState == null){
            extras=getIntent().getExtras();
            if(extras!=null){
                guestMode=extras.getBoolean("Guest_mode");
                if(!guestMode) {
                    PROFILE = extras.getString("profile_id");
                    NAME = extras.getString("profile_name");
                }
                else{
                    PROFILE=null;
                    NAME="Logged in as a Guest";
                }
            }
        }
        else {
            guestMode=savedInstanceState.getBoolean("Guest_mode");
            if(!guestMode) {
                PROFILE = (String) savedInstanceState.getSerializable("profile_id");
                NAME = (String) savedInstanceState.getSerializable("profile_name");
            }
            else{
                PROFILE=null;
                NAME="Logged in as a Guest";
            }
        }*/
        profile=Profile.getCurrentProfile();
        if(profile==null){
            guestMode=true;
            PROFILE=null;
            NAME="Logged in as a Guest";
        }
        else{
            guestMode=false;
            PROFILE =profile.getId();
            NAME =profile.getName();
        }
        /*lv_sheep = (ListView) findViewById(R.id.listView);
        lv_sheep.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(home_screen.this,event_discription.class);
                String title=((TextView)view.findViewById(R.id.tv_title)).getText().toString();
                String description=((TextView)view.findViewById(R.id.tv_disc)).getText().toString();
                String date=((TextView)view.findViewById(R.id.tv_date)).getText().toString();
                intent.putExtra("Guest_mode",guestMode );
                intent.putExtra("TITLE",title);
                intent.putExtra("DESC",description);
                intent.putExtra("DATE",date);
                intent.putExtra("CATEGORY",values.get((position)).category);
                intent.putExtra("CREATOR_ID",values.get((position)).creator_id);
                intent.putExtra("TIME",values.get((position)).time);
                intent.putExtra("EVENT_ID",values.get((position)).event_id);
                home_screen.this.startActivity(intent);
                home_screen.this.finish();
            }
        });
        values=new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("EventsTable");
        FindCallback<ParseObject> callback=new FindCallback<ParseObject>() {

            public void done(List<ParseObject> eventsList, com.parse.ParseException e) {
                if (e == null) {
                    Log.d("EVENT_CREATE", "Retrieved " + eventsList.size() + " events");
                    for (int index = eventsList.size()-1; index >= 0; index--) {
                        values.add(new ListRow((String) eventsList.get(index).get("EventName"),
                                (String) eventsList.get(index).get("EventDate"),
                                (String) eventsList.get(index).get("EventDesc"),
                                (String) eventsList.get(index).get("profileId"),
                                (String) eventsList.get(index).get("EventCat"),
                                (String) eventsList.get(index).get("EventTime"),
                                (String) eventsList.get(index).getObjectId()));
                    }
                    adapter = new MyAdapter1(values,MyAdapter1.ListType.EVENT);
                    lv_sheep.setAdapter(adapter);

                } else {
                    Log.d("EVENT_CREATE", "Error: ");
                }
            }
        };

        query.findInBackground(callback);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(lv_sheep);
        */

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);




        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new MyAdapter(TITLES,ICONS,NAME,PROFILE,getApplicationContext(),guestMode);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.nav_bar_open,R.string.nav_bar_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State
        /*
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!guestMode) {
                    Intent intent = new Intent(getApplicationContext(), event_create.class);
                    intent.putExtra("EDIT_MODE",false);
                    startActivity(intent);
                    home_screen.this.finish();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Login to add events", Toast.LENGTH_SHORT).show();
                }
            }
        });
     */

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
