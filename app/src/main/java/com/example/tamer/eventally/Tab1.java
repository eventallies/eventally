package com.example.tamer.eventally;

/**
 * Created by Tamer on 5/19/2015.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.Profile;
import com.melnykov.fab.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


public class Tab1 extends Fragment {
    private ListView lv_sheep;
    ArrayList<ListRow> values;
    String NAME = "Guest";
    String PROFILE;
    static Boolean guestMode;
    Profile profile;
    MyAdapter1 adapter;

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate (savedInstanceState);
        values=new ArrayList<>();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("EventsTable");
        FindCallback<ParseObject> callback=new FindCallback<ParseObject>() {

            public void done(List<ParseObject> eventsList, com.parse.ParseException e) {
                if (e == null) {
                    Log.d("EVENT_CREATE", "Retrieved " + eventsList.size() + " events");
                    for (int index = eventsList.size()-1; index >= 0; index--) {
                        values.add(new ListRow((String) eventsList.get(index).get("EventName"),
                                (String) eventsList.get(index).get("EventDate"),
                                (String) eventsList.get(index).get("EventDesc"),
                                (String) eventsList.get(index).get("profileId"),
                                (String) eventsList.get(index).get("EventCat"),
                                (String) eventsList.get(index).get("EventTime"),
                                eventsList.get(index).getObjectId()));
                    }
                    adapter = new MyAdapter1(values,MyAdapter1.ListType.EVENT);
                    lv_sheep.setAdapter(adapter);

                } else {
                    Log.d("EVENT_CREATE", "Error: ");
                }
            }
        };

        query.findInBackground(callback);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.tab_1,container,false);

        profile= Profile.getCurrentProfile();
        if(profile==null){
            guestMode=true;
            PROFILE=null;
            NAME="Logged in as a Guest";
        }
        else{
            guestMode=false;
            PROFILE =profile.getId();
            NAME =profile.getName();
        }
        lv_sheep = (ListView) v.findViewById(R.id.listView);
        lv_sheep.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), event_discription.class);
                String title = ((TextView) view.findViewById(R.id.tv_title)).getText().toString();
                String description = ((TextView) view.findViewById(R.id.tv_disc)).getText().toString();
                String date = ((TextView) view.findViewById(R.id.tv_date)).getText().toString();
                intent.putExtra("Guest_mode", guestMode);
                intent.putExtra("TITLE", title);
                intent.putExtra("DESC", description);
                intent.putExtra("DATE", date);
                intent.putExtra("CATEGORY", values.get((position)).category);
                intent.putExtra("CREATOR_ID", values.get((position)).creator_id);
                intent.putExtra("TIME", values.get((position)).time);
                intent.putExtra("EVENT_ID", values.get((position)).event_id);
                getActivity().startActivity(intent);

            }
        });



        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.attachToListView(lv_sheep);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!guestMode) {
                    Intent intent = new Intent(getActivity(), event_create.class);
                    intent.putExtra("EDIT_MODE", false);
                    startActivity(intent);

                } else {
                    Toast.makeText(getActivity(), "Login to add events", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }
}