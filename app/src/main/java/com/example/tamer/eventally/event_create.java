package com.example.tamer.eventally;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class event_create extends ActionBarActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemSelectedListener {
    final Calendar myCalendar = Calendar.getInstance();
    private Button b_date;
    private Button b_time;
    private Button b_place;
    private EditText et_title;
    private EditText et_desc;
    private Spinner spinner;
    private String event_id;
    Profile profile;
    String category_selected;
    Boolean edit_mode;
    private RadioGroup rg_pp;
    private boolean addEventEnabled = false;
    private boolean exist_date = false;
    private boolean exist_time = false;
    private boolean exist_place = false;
    private boolean exist_title = false;
    private boolean exist_desc = false;
    private boolean exist_category = false;
    private boolean exist_radio = false;
    private MenuItem i_add_event;


    int PLACE_PICKER_REQUEST = 1;
    private GoogleApiClient mGoogleApiClient;

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        b_date.setText(sdf.format(myCalendar.getTime()));
    }

    private int getCategoryPosition(String category) {
        switch (category) {
            case "Sports":
                return 0;
            case "Lan Party":
                return 1;
            case "Shopping":
                return 2;
            case "Study Group":
                return 3;
        }
        return -1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_create);
        b_date = (Button) findViewById(R.id.b_date_create);
        b_time = (Button) findViewById(R.id.b_time_create);
        b_place = (Button) findViewById(R.id.b_location_create);
        et_title = (EditText) findViewById(R.id.et_title_create);
        et_desc = (EditText) findViewById(R.id.et_desc_create);
        spinner = (Spinner) findViewById(R.id.sp_category);
        rg_pp = (RadioGroup) findViewById(R.id.rg_private_public);


        if (savedInstanceState != null) {
            exist_date = savedInstanceState.getBoolean("exist_date_state");
            exist_time = savedInstanceState.getBoolean("exist_time_state");
            exist_desc = savedInstanceState.getBoolean("exist_desc_state");
            exist_place = savedInstanceState.getBoolean("exist_place_state");
            exist_title = savedInstanceState.getBoolean("exist_title_state");
            exist_category = savedInstanceState.getBoolean("exist_category_state");
            exist_radio = savedInstanceState.getBoolean("exist_radio_state");
            addEventEnabled = savedInstanceState.getBoolean("addEventEnabled_state");
        }

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        /***
         * initializing spinner and setting it's adapter
         */

        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.category_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        /***
         * initializing spinner and setting it's adapter
         */

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            edit_mode = extras.getBoolean("EDIT_MODE");
        }
        final Intent intent = getIntent();
        if (edit_mode) {
            final String title = intent.getStringExtra("TITLE");
            final String desc = intent.getStringExtra("DESC");
            final String date = intent.getStringExtra("DATE");
            final String time = intent.getStringExtra("TIME");
            final String category = intent.getStringExtra("CATEGORY");
            event_id = intent.getStringExtra("EVENT_ID");
            exist_date = true;
            exist_time = true;
            exist_place = true;
            exist_title = true;
            exist_desc = true;
            exist_category = true;
            exist_radio = true;
            et_title.setText(title);
            et_desc.setText(desc);
            b_date.setText(date);
            b_time.setText(time);
            spinner.setSelection(getCategoryPosition(category));
        }

        /** radio group change */
        rg_pp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                exist_radio = true;
                updateAddEventEnabled();
            }
        });

        et_desc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null || s.length() == 0) {
                    exist_desc = false;
                } else if (s.length() > 0) {
                    exist_desc = true;
                }
                updateAddEventEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null || s.length() == 0) {
                    exist_title = false;
                } else if (s.length() > 0) {
                    exist_title = true;
                }
                updateAddEventEnabled();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /***
         * Date picker and the click listener
         */
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();

                exist_date = true;
                updateAddEventEnabled();
            }

        };
        b_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(event_create.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        /***
         * Date picker and the click listener
         */

        /***
         * Timer picker and the click listener
         */
        b_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(event_create.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        b_time.setText(selectedHour + ":" + selectedMinute);
                        exist_time = true;
                        updateAddEventEnabled();
                    }
                }, hour, minute, true);
                mTimePicker.show();
            }
        });
        /***
         * Timer picker and the click listener
         */

        /**
         * Location set
         */
        b_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

                Context context = getApplicationContext();
                try {
                    startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_create, menu);
        i_add_event = menu.findItem(R.id.action_event_add);
        updateAddEventEnabled();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_event_add) {
            if (!edit_mode) {
                Log.d("CREATE", "got in if");
                ParseObject eventObject = new ParseObject("EventsTable");
                profile = Profile.getCurrentProfile();

                eventObject.put("profileId", profile.getId());
                eventObject.put("EventName", et_title.getText().toString());
                eventObject.put("EventDate", b_date.getText().toString());
                eventObject.put("EventPlace",b_place.getText().toString());
                eventObject.put("EventDesc", et_desc.getText().toString());
                eventObject.put("EventCat", category_selected);
                eventObject.put("EventTime", b_time.getText());
                try {
                    eventObject.save();
                } catch (ParseException e) {
                    Log.d("PARSE_ERROR", "couldn't save!");
                }


            } else {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("EventsTable");
// Retrieve the object by id
                Log.d("CREATE", event_id);
                query.getInBackground(event_id, new GetCallback<ParseObject>() {
                    public void done(ParseObject event, ParseException e) {

                        if (e == null) {
                            // Now let's update it with some new data. In this case, only cheatMode and score
                            // will get sent to the Parse Cloud. playerName hasn't changed.
                            event.put("EventName", et_title.getText().toString());
                            event.put("EventDesc", et_desc.getText().toString());
                            event.put("EventCat", category_selected);
                            event.put("EventPlace",b_place.getText().toString());
                            event.put("EventDate", b_date.getText().toString());
                            event.put("EventTime", b_time.getText().toString());
                            Log.d("CREATE", "reached put");
                            //need to also add location here
                            try {
                                event.save();
                            } catch (ParseException e1) {
                                Log.d("CREATE", "update failed");
                            }
                        }
                    }
                });
            }
            Intent intent = new Intent(event_create.this, home_screen.class);
            startActivity(intent);
            event_create.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        category_selected = parent.getItemAtPosition(position).toString();
        exist_category = true;
        updateAddEventEnabled();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        exist_category = false;
        updateAddEventEnabled();
    }

    private void updateAddEventEnabled() {
        addEventEnabled = exist_date && exist_time && exist_title && exist_place && exist_desc && exist_radio && exist_category;
        i_add_event.setEnabled(addEventEnabled);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("exist_date_state", exist_date);
        outState.putBoolean("exist_time_state", exist_time);
        outState.putBoolean("exist_desc_state", exist_desc);
        outState.putBoolean("exist_place_state", exist_place);
        outState.putBoolean("exist_title_state", exist_title);
        outState.putBoolean("exist_category_state", exist_category);
        outState.putBoolean("exist_radio_state", exist_radio);
        outState.putBoolean("addEventEnabled_state", addEventEnabled);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                b_place.setText(name.toString() +"\n"+ address.toString());
                exist_place=true;
                updateAddEventEnabled();
            }
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
