package com.example.tamer.eventally;

/**
 * Created by Tamer on 4/24/2015.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;

import org.w3c.dom.Text;

/**
 * Created by hp1 on 28-12-2014.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_TEXT=2;
    private static final int TYPE_DIV=3;

    private String mNavTitles[]; // String Array to store the passed titles Value from MainActivity.java
    private int mIcons[];       // Int Array to store the passed icons resource value from MainActivity.java

    private String name;        //String Resource for header View Name
    private String profile;        //profile id that was received from LogInActivity
    public Context my_context;
    public Boolean guest_mode;
    public View header_view;


    // Creating a ViewHolder which extends the RecyclerView View Holder
    // ViewHolder are used to to store the inflated views in order to recycle them

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
        int Holderid;

        TextView textView;
        ImageView imageView;
        ProfilePictureView profile;
        TextView Name;
        TextView section;
        Context viewHolder_my_context;
        Boolean holder_guest_mode;
        View holder_header_view;

        public void set_header_view(View header_view){
            holder_header_view=header_view;
        }
        public ViewHolder(View itemView,int ViewType,Context context,Boolean holder_guest_mode) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
            super(itemView);
            this.holder_guest_mode=holder_guest_mode;
            this.viewHolder_my_context = context;
            if(ViewType==TYPE_ITEM) {
                itemView.setClickable(true);
                itemView.setOnClickListener(this);
            }
            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if(ViewType == TYPE_ITEM) {
                textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from item_row.xml
                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from item_row.xml
                Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
            }
            else if (ViewType == TYPE_HEADER){


                Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
                profile = (ProfilePictureView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic
                Holderid = 0;                                                // Setting holder id = 0 as the object being populated are of type header view
            }
            else if (ViewType == TYPE_TEXT){
                section=(TextView) itemView.findViewById(R.id.tv_section);
                Holderid = 2;
            }
            else {
                Holderid=3;
            }
        }
        @Override
        public void onClick(View v) {
            if(getPosition()==9 && !holder_guest_mode){

                LoginManager.getInstance().logOut();
                textView=(TextView) v.findViewById(R.id.rowText);
                textView.setText("Log in");
                holder_guest_mode=true;
                home_screen.guestMode=true;
                Intent intent=new Intent(viewHolder_my_context,LogInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                viewHolder_my_context.startActivity(intent);
                Toast.makeText(viewHolder_my_context, "Successfully logged out", Toast.LENGTH_SHORT).show();
            }
            else if(getPosition()==9 && holder_guest_mode){
                Intent intent=new Intent(viewHolder_my_context,LogInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                viewHolder_my_context.startActivity(intent);
            }
            else {
                Toast.makeText(viewHolder_my_context, "The Item Clicked is: " + getPosition(), Toast.LENGTH_SHORT).show();
            }
        }



    }



    MyAdapter(String Titles[],int Icons[],String Name, String Profile,Context context,Boolean guestmode){ // MyAdapter Constructor with titles and icons parameter
        // titles, icons, name, email, profile pic are passed from the main activity as we
        mNavTitles = Titles;                //have seen earlier
        mIcons = Icons;
        name = Name;
        profile = Profile;                     //here we assign those passed values to the values we declared here
        my_context=context;
        this.guest_mode=guestmode;
        //in adapter



    }



    //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_item_row,parent,false); //Inflating the layout

            ViewHolder vhItem = new ViewHolder(v,viewType,my_context,guest_mode); //Creating ViewHolder and passing the object of type view

            return vhItem; // Returning the created object

            //inflate your layout and pass it to view holder

        } else if (viewType == TYPE_HEADER) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false); //Inflating the layout
            header_view=v;
            ViewHolder vhHeader = new ViewHolder(v,viewType,my_context,guest_mode); //Creating ViewHolder and passing the object of type view
            return vhHeader; //returning the object created


        }
        else if(viewType == TYPE_TEXT){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_section,parent,false);
            ViewHolder vhSection = new ViewHolder(v,viewType,my_context,guest_mode);
            return vhSection;
        }
        else if(viewType == TYPE_DIV){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.drawer_divider,parent,false);
            ViewHolder vhSection = new ViewHolder(v,viewType,my_context,guest_mode);
            return vhSection;
        }

        return null;

    }

    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        if(holder.Holderid ==1) {                              // as the list view is going to be called after the header view so we decrement the
            // position by 1 and pass it to the holder while setting the text and image
            if(position<=6) {
                holder.textView.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
                holder.imageView.setImageResource(mIcons[position - 1]);// Settimg the image with array of our icons
            }
            if(position==9 && guest_mode){
                holder.textView.setText("Log in");
                holder.imageView.setImageResource(mIcons[position - 2]);
            }
            else if(position>=8){
                holder.textView.setText(mNavTitles[position - 2]); // Setting the Text with the array of our Titles
                holder.imageView.setImageResource(mIcons[position - 2]);// Settimg the image with array of our icons
            }
        }
        else if(holder.Holderid ==2) {
            holder.section.setText("Filter");
        }
        else if(holder.Holderid ==0){
            if(profile!=null) {
                holder.profile.setProfileId(profile);          // Similarly we set the resources for header view
            }
            holder.Name.setText(name);
        }

    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return mNavTitles.length+2; // the number of items in the list will be +1 the titles including the header view.
    }


    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
       else if(position == 1) {
            return TYPE_ITEM;
        }
        else if(position == 3 || position== 7){
            return TYPE_DIV;
        }
        else {
            return TYPE_ITEM;
        }
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
}


}