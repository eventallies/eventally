package com.example.tamer.eventally;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Tamer on 4/28/2015.
 */
public class MyAdapter1 extends BaseAdapter {
    public enum ListType {
        COMMENT, EVENT
    }

    private ArrayList<ListRow> myList;
    private ListType list_type;
    private ImageLoader image_loader;
    public MyAdapter1(ArrayList<ListRow> aList,ListType lt) {
        myList = aList;
        this.list_type=lt;
        image_loader=ImageLoader.getInstance();
    }


    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        ViewHolder viewHolder;

        Log.d("MY_TAG", "Position: " + position);

        if (convertView == null) {
            if(list_type==ListType.EVENT) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listrow, parent, false);
            }
            else{
                Log.d("OTHER_VIEW", "asdasdasd");
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listrow_comment, parent, false);
            }

            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.tv_title);
            viewHolder.date = (TextView) view.findViewById(R.id.tv_date);
            viewHolder.discription = (TextView) view.findViewById(R.id.tv_disc);
            viewHolder.image=(CircleImageView) view.findViewById(R.id.cv_row);
            view.setTag(viewHolder);

        }
        else {
            view = convertView;

            viewHolder = (ViewHolder) view.getTag();
        }

        // Put the content in the view
        viewHolder.title.setText(myList.get(position).title);
        viewHolder.date.setText(myList.get(position).date);
        viewHolder.discription.setText(myList.get(position).discripton);
        String img_value ="http://graph.facebook.com/"+myList.get(position).creator_id+"/picture?type=large";
        DisplayImageOptions options = new DisplayImageOptions.Builder()
        .cacheInMemory(true).cacheOnDisk(true).build();
        image_loader.displayImage(img_value,viewHolder.image,options);
        return view;
    }



    private class ViewHolder {
        TextView title;
        TextView date;
        TextView discription;
        CircleImageView image;
    }


}
