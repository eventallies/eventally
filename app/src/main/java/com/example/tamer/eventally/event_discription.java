package com.example.tamer.eventally;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.facebook.Profile;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class event_discription extends ActionBarActivity {
    private ListView lv_comments;
    ArrayList<ListRow> values;
    MyAdapter1 adapter;
    TextView tv_title;
    TextView tv_date;
    TextView tv_desc;
    TextView tv_category;
    Button b_join_edit;
    Boolean is_creator=false;
    TextView tv_event_time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_discription);

        View header = getLayoutInflater().inflate(R.layout.list_header, null);

        tv_title = (TextView) header.findViewById(R.id.tv_desc_title);
        tv_desc = (TextView) header.findViewById(R.id.tv_desc_desc);
        tv_date = (TextView) header.findViewById(R.id.tv_desc_date);
        tv_category= (TextView) header.findViewById(R.id.tv_desc_category);
        b_join_edit= (Button) header.findViewById(R.id.b_dec_joinEdit);
        tv_event_time=(TextView) header.findViewById(R.id.tv_desc_time);
        Intent intent = getIntent();
        final String title    =   intent.getStringExtra("TITLE");
        final String desc     =   intent.getStringExtra("DESC");
        final String date     =   intent.getStringExtra("DATE");
        final String category =   intent.getStringExtra("CATEGORY");
        final String creator_id = intent.getStringExtra("CREATOR_ID");
        final String time     =   intent.getStringExtra("TIME");
        final String event_id     =   intent.getStringExtra("EVENT_ID");
        Boolean guest_mode=   intent.getBooleanExtra("Guest_mode",true);
        tv_title.setText(title);
        tv_desc.setText(desc);
        tv_date.setText(date);
        tv_category.setText(category);
        tv_event_time.setText(time);
        Profile profile = Profile.getCurrentProfile();
        if(!guest_mode) {
            if (profile.getId().equals(creator_id)) {
                b_join_edit.setText("Edit");
                is_creator = true;
            } else {
                is_creator = false;
            }
        }
            b_join_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(is_creator){
                        Intent intent=new Intent(event_discription.this,event_create.class);
                        intent.putExtra("TITLE",title);
                        intent.putExtra("DESC",desc);
                        intent.putExtra("DATE",date);
                        intent.putExtra("CATEGORY",category);
                        intent.putExtra("EDIT_MODE",true);
                        intent.putExtra("TIME",time);
                        intent.putExtra("EVENT_ID",event_id);
                        startActivity(intent);
                        event_discription.this.finish();
                    }
                }
            });


        setCategoryColor(category,header);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        lv_comments = (ListView) findViewById(R.id.lv_comments);

        values = new ArrayList<>();


        lv_comments.addHeaderView(header);

        adapter = new MyAdapter1(values, MyAdapter1.ListType.COMMENT);
        lv_comments.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_discription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setCategoryColor(String category,View header){
        Button b_attend= (Button) header.findViewById(R.id.b_desc_attend);
        LinearLayout ll_header =(LinearLayout) header.findViewById(R.id.ll_header);
        Button b_join_edit= (Button) header.findViewById(R.id.b_dec_joinEdit);
        ImageView iv_category_icon= (ImageView) header.findViewById(R.id.iv_desc_category_icon);
        TextView tv_desc_category=(TextView) header.findViewById(R.id.tv_desc_category);

        int category_background_color;
        int category_buttons_color;
        int category_image;


        switch (category){
            case "Sports":
                category_background_color=getResources().getColor(R.color.categorySport);
                category_buttons_color=getResources().getColor(R.color.categorySportButton);
                category_image=R.mipmap.ic_maps_directions_bike;
                changeColors(ll_header,b_attend,b_join_edit,tv_desc_category,iv_category_icon,category_background_color,category_buttons_color,category_image);

                break;
            case "Lan Party":
                category_background_color=getResources().getColor(R.color.categoryLanParty);
                category_buttons_color=getResources().getColor(R.color.categoryLanPartyButton);
                category_image=R.mipmap.ic_hardware_laptop_mac;
                changeColors(ll_header, b_attend, b_join_edit, tv_desc_category, iv_category_icon, category_background_color, category_buttons_color, category_image);
                break;
            case "Shopping":
                category_background_color=getResources().getColor(R.color.categoryShopping);
                category_buttons_color=getResources().getColor(R.color.categoryShoppingButton);
                category_image=R.mipmap.ic_action_add_shopping_cart;
                changeColors(ll_header, b_attend, b_join_edit, tv_desc_category, iv_category_icon, category_background_color, category_buttons_color, category_image);
                break;
            case "Study Group":
                category_background_color=getResources().getColor(R.color.categoryStudyGroup);
                category_buttons_color=getResources().getColor(R.color.categoryStudyGroupButton);
                category_image=R.mipmap.ic_maps_local_library;
                changeColors(ll_header, b_attend, b_join_edit, tv_desc_category, iv_category_icon, category_background_color, category_buttons_color, category_image);
                break;

        }
    }
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void changeStatusBarColor(int color) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = event_discription.this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(color);

        }
    }
    public void changeColors(LinearLayout ll_header,Button b_attend,Button b_join_edit,TextView tv_desc_title,ImageView iv_category_icon, int category_background_color,int category_buttons_color,int category_image){

        ll_header.setBackgroundColor(category_background_color);
        b_attend.setBackgroundColor(category_buttons_color);
        b_join_edit.setBackgroundColor(category_buttons_color);
        tv_desc_title.setBackgroundColor(category_background_color);
        iv_category_icon.setImageResource(category_image);
        changeStatusBarColor(category_background_color);
    }
}
