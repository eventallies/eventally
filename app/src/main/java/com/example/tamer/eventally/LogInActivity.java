package com.example.tamer.eventally;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;


public class LogInActivity extends ActionBarActivity {
    private LoginButton fb_login;
    private CallbackManager mycbManager;
    private Profile profile;
    private Button b_skip;

    private FacebookCallback<LoginResult> myCallBack=new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            profile=Profile.getCurrentProfile();
            if(profile==null){
                Toast.makeText(getApplicationContext(),"Error ",Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Welcome " + profile.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LogInActivity.this, home_screen.class);
                intent.putExtra("profile_id", profile.getId());
                intent.putExtra("profile_name", profile.getName());
                intent.putExtra("Guest_mode", false);
                startActivity(intent);
                LogInActivity.this.finish();
            }
        }

        @Override
        public void onCancel() {
            Toast.makeText(getApplicationContext(),"canceled",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException e) {
            Toast.makeText(getApplicationContext(),"Facebook error",Toast.LENGTH_SHORT).show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_log_in);

        mycbManager=CallbackManager.Factory.create();
        fb_login=(LoginButton) findViewById(R.id.fb_login_button);
        if(fb_login.getText().equals("Log out")) {
            profile=Profile.getCurrentProfile();
            if(profile==null){
                Toast.makeText(getApplicationContext(),"Error null profile",Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intent = new Intent(LogInActivity.this, home_screen.class);
                intent.putExtra("profile_id", profile.getId());
                intent.putExtra("profile_name", profile.getName());
                intent.putExtra("Guest_mode", false);
                startActivity(intent);
                LogInActivity.this.finish();
            }
        }
        fb_login.registerCallback(mycbManager,myCallBack);
        b_skip=(Button) findViewById(R.id.b_skip);
        b_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LogInActivity.this,home_screen.class);
                intent.putExtra("Guest_mode",true);
                startActivity(intent);
                LogInActivity.this.finish();
            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_log_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mycbManager.onActivityResult(requestCode, resultCode, data))
            return;
    }
}
